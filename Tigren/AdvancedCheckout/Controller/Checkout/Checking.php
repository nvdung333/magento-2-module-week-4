<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Controller\Checkout;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Catalog\Model\ProductRepository;

/**
 * Class Checking
 * @package Tigren\AdvancedCheckout\Controller\Checkout
 */
class Checking extends Action
{
    /**
     * @var Session
     */
    protected $_session;

    /**
     * @var Item
     */
    protected $_quoteItem;

    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     * Checking constructor.
     * @param Context $context
     * @param Session $session
     * @param Item $quoteItem
     * @param ProductRepository $productRepository
     */
    function __construct(
        Context $context,
        Session $session,
        Item $quoteItem,
        ProductRepository $productRepository
    )
    {
        $this->_quoteItem = $quoteItem;
        $this->_session = $session;
        $this->_productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $postValue = $this->getRequest()->getPostValue();
        $productId = $postValue['id_item'];
        $product = $this->_productRepository->getById($productId);
        $attributes = $product->getCustomAttribute('allow_multi_order');
        if (!empty($attributes)) {
            $attributeValue = $attributes->getValue();
            echo $attributeValue;
        } else {
            echo "0";
        }
    }
}
