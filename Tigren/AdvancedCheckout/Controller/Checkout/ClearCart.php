<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Controller\Checkout;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use \Magento\Quote\Model\Quote\Item;
use Exception;

/**
 * Class ClearCart
 * @package Tigren\AdvancedCheckout\Controller\Checkout
 */
class ClearCart extends Action
{
    /**
     * @var Session
     */
    protected $_session;

    /**
     * @var Item
     */
    protected $_quoteItem;

    /**
     * ClearCart constructor.
     * @param Context $context
     * @param Session $session
     * @param Item $quoteItem
     */
    function __construct(
        Context $context,
        Session $session,
        Item $quoteItem
    )
    {
        $this->_quoteItem = $quoteItem;
        $this->_session = $session;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $allItems = $this->_session->getQuote()->getAllVisibleItems();
        $quoteId = $this->_session->getQuoteId();
        $error = 0;
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            $allItems = $this->_quoteItem->load($itemId);
            try {
                $allItems->delete();
            } catch (Exception $e) {
                $error++;
            }
        }
        if(!empty($quoteId)){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('quote');
            $sql = "DELETE  FROM " . $tableName." WHERE entity_id = ".$quoteId;
            $connection->query($sql);
        }
        if ($error) {
            $this->messageManager->addErrorMessage("fail ". $error." item");
            echo "error";
        } else {
            echo "success";
        }
    }
}
